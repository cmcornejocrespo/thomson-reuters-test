package com.thomsonreuters.game.deck;

import com.thomsonreuters.domain.card.Card;
import com.thomsonreuters.domain.card.Numeral;
import com.thomsonreuters.domain.card.Suit;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultShuffleUnitTest {

    private DefaultShuffle sut;

    @Before
    public void setUp() {

        sut = new DefaultShuffle("Any Shuffle Strategy");
    }

    @Test
    public void itShouldShuffleCards() {

        //when
        sut.shuffle();

        final Card cardAfterShuffle = sut.dealCard();

        //then
        assertThat(cardAfterShuffle).isNotEqualTo(new Card(Suit.SPADES, Numeral.ACE));

    }
}
